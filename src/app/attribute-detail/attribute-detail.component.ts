import {Component, Input, OnInit, ViewChild} from '@angular/core';
import {Location} from '@angular/common';
import {Attribute} from '../dtos/attribute';
import {ActivatedRoute, Router} from '@angular/router';
import {UserService} from '../services/user.service';
import {User} from '../dtos/User';
import {Sheet} from '../dtos/Sheet';
import {AttributeBlock} from '../dtos/AttributeBlock';
import Utils from '../utils';
import {MatDialog, MatSnackBar} from '@angular/material';
import {BasicDialogComponent} from '../basic-dialog/basic-dialog.component';
import {SessionService} from '../services/session.service';
import {ToolbarService} from '../services/toolbar.service';
import {NgForm} from '@angular/forms';

@Component({
  selector: 'app-attribute-detail',
  templateUrl: './attribute-detail.component.html',
  styleUrls: ['./attribute-detail.component.css']
})
export class AttributeDetailComponent implements OnInit {
  user: User;
  originalAttribute: Attribute;
  attribute: Attribute;
  sheet: Sheet;
  block: AttributeBlock;
  sheetId: string;
  @ViewChild(NgForm)attrForm: NgForm;

  constructor( private route: ActivatedRoute,
               private router: Router,
               private location: Location,
               private userService: UserService,
               private sessionService: SessionService,
               private toolbarService: ToolbarService,
               public dialog: MatDialog,
               public snackBar: MatSnackBar) { }

  ngOnInit() {
    this.initializeUser();
    this.getAttribute();
    this.toolbarService.addEditionToolbar("attributeDetail", "Nuevo atributo", "Editar atributo", this, this.attribute);
    this.sessionService.loadingPage("attributeDetail");
  }

  getAttribute(): void {
    let id = this.route.snapshot.paramMap.get('id');
    const blockId= this.route.snapshot.paramMap.get('blockId');
    this.sheetId= this.route.snapshot.paramMap.get('sheetId');

    this.sheet= this.user.sheets.find(x => x.id === this.sheetId.toString());
    this.block=this.sheet.blocks.find(x => x.id === blockId.toString());

    if (id === 'new') {
      this.attribute= new class implements Attribute {
        bonus: number;
        code: string;
        counter: number;
        id: string;
        name: string;
        notes: string;
        value: number;
      };
      this.attribute.name="";
      this.attribute.value=0;
      this.attribute.code="";
      this.attribute.bonus=0;
      this.attribute.counter=0;
      this.attribute.notes="";
    }else{
      this.originalAttribute= this.block.attributes.find(x => x.id === id.toString());
      this.attribute= Object.assign({},this.originalAttribute);
    }
  }

  initializeUser(): void { //TODO llevar esto a una clase base
    this.user = this.userService.loggedUser;
    if (!this.userService.loggedUser) {
      this.router.navigate(['/login']).then();
    }
  }

  save(): void {
    if(this.attrForm.valid) {
      console.log("Saving attribute...");
      if (this.attribute.id == null) {
        this.attribute.id = Utils.generateId().toString();
        this.block.attributes.push(this.attribute);
      } else {
        this.originalAttribute.name = this.attribute.name;
        this.originalAttribute.code = this.attribute.code;
        this.originalAttribute.value = this.attribute.value;
        this.originalAttribute.bonus = this.attribute.bonus;
        this.originalAttribute.counter = this.attribute.counter;
        this.originalAttribute.notes = this.attribute.notes;
      }
      this.userService.updateUser(this.user.id, this.user).then(x => {
        this.snackBar.open("Atributo guardado!", "Ok", {
          duration: 3000,
        });
        this.router.navigate(['/sheet/' + this.sheetId]).then();
      });
    }else{
      this.snackBar.open("Por favor, verifica los datos ingresados...", "Ok", {
        duration: 3000,
      });
    }
  }

  delete(id: string, name:string): void {

    const dialogRef = this.dialog.open(BasicDialogComponent, {
      data: {
        title: "Eliminar atributo",
        content: "Está seguro que desea eliminar el atributo " +name + "? ",
        actions:true
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      if(result) {
        this.block.attributes= this.block.attributes.filter(x => x.id != id.toString());
        this.userService.updateUser(this.user.id, this.user).then(x => {
          this.snackBar.open("El atributo ha sido eliminado", "Ok", {
            duration: 3000,
          });
          this.router.navigate(['/sheet/'+this.sheetId]).then();
        });
      }else{
        this.snackBar.open("Hubo un problema al eliminar el atributo, por favor inténtalo nuevamente.", "Ok", {
          duration: 3000,
        });
      }

    });


  }
}





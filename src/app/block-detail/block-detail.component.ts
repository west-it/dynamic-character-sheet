import {Component, OnInit, ViewChild} from '@angular/core';
import {User} from '../dtos/User';
import {Attribute} from '../dtos/attribute';
import {Sheet} from '../dtos/Sheet';
import {AttributeBlock} from '../dtos/AttributeBlock';
import {ActivatedRoute, Router} from '@angular/router';
import {Location} from '@angular/common';
import {UserService} from '../services/user.service';
import {BlockColumns} from '../dtos/BlockColumns';
import Utils from '../utils';
import {BasicDialogComponent} from '../basic-dialog/basic-dialog.component';
import {MatDialog, MatSnackBar} from '@angular/material';
import {SessionService} from '../services/session.service';
import {ToolbarService} from '../services/toolbar.service';
import {NgForm} from '@angular/forms';

@Component({
  selector: 'app-block-detail',
  templateUrl: './block-detail.component.html',
  styleUrls: ['./block-detail.component.css']
})
export class BlockDetailComponent implements OnInit {
  user: User;
  originalBlock: AttributeBlock;
  block: AttributeBlock;
  sheet: Sheet;
  sheetId: string;
  allColumns:BlockColumns[];
  selectedColumns:string[]=["Nombre"];//todo cargar con propertie
  @ViewChild(NgForm)blockForm: NgForm;


  constructor(private route: ActivatedRoute,
              private router: Router,
              private location: Location,
              private userService: UserService,
              private sessionService: SessionService,
              private toolbarService: ToolbarService,
              public dialog: MatDialog,
              public snackBar: MatSnackBar) { }

  ngOnInit() {
    this.initializeUser();
    this.allColumns=Utils.getColumns();//all columns
    this.getBlock();
    //first set the toolbar, then call to loadingPage
    this.toolbarService.addEditionToolbar("blockDetail", "Nuevo bloque","Editar bloque", this, this.block);
    this.sessionService.loadingPage("blockDetail");

  }

  getBlock(): void {
    const blockId= this.route.snapshot.paramMap.get('blockId');
    this.sheetId= this.route.snapshot.paramMap.get('sheetId');

    this.sheet= this.user.sheets.find(x => x.id === this.sheetId.toString());
    if(this.sheet.blocks)
      this.block=this.sheet.blocks.find(x => x.id === blockId.toString());

    if (blockId === 'new') {
      this.block= Utils.generateNewBlock();
    }else{
      this.originalBlock= this.sheet.blocks.find(x => x.id === blockId.toString());
      this.block= Object.assign({},this.originalBlock);
      this.selectedColumns=Utils.getColumnsValues(this.block.visibleColumns);//selected columns
    }
  }

  initializeUser(): void { //TODO llevar esto a una clase base
    this.user = this.userService.loggedUser;
    if (!this.userService.loggedUser) {
      this.router.navigate(['/login']).then();
    }
  }

  save(): void {
    if(this.blockForm.valid) {
      console.log("Saving block...");

      let colCodes: string[] = [];
      this.selectedColumns.forEach(function (col) {
        colCodes.push(Utils.getColumns().find(x => x.name === col).code);
      });
      this.block.visibleColumns = colCodes;
      console.log(colCodes);

      if (this.block.id == null) {
        this.block.id = Utils.generateId().toString();
        if(!this.sheet.blocks)
          this.sheet.blocks=[];
        this.sheet.blocks.push(this.block);
      } else {
        this.originalBlock.name = this.block.name;
        this.originalBlock.description = this.block.description;
        this.originalBlock.visibleColumns = this.block.visibleColumns;
      }

      this.userService.updateUser(this.user.id, this.user).then(attribute => {
        this.snackBar.open("Bloque guardado!", "Ok", {
          duration: 3000,
        });
        this.router.navigate(['/sheet/' + this.sheetId]).then();
      });
    }else{
      this.snackBar.open("Por favor, verifica los datos ingresados...", "Ok", {
        duration: 3000,
      });
    }
  }

  delete(id: string, name:string): void {

    const dialogRef = this.dialog.open(BasicDialogComponent, {
      data: {
        title: "Eliminar bloque",
        content: "Está seguro que desea eliminar el bloque " + name + "? ",
        actions: true
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.sheet.blocks = this.sheet.blocks.filter(x => x.id != id.toString());
        this.userService.updateUser(this.user.id, this.user).then(x => {
          this.snackBar.open("El bloque ha sido eliminado", "Ok", {
            duration: 3000,
          });
          this.router.navigate(['/sheet/' + this.sheetId]).then();
        });
      }else{
        this.snackBar.open("Hubo un problema al eliminar el bloque, por favor inténtalo nuevamente.", "Ok", {
          duration: 3000,
        });
      }
    });

  }

}

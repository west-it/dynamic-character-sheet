import {Attribute} from './attribute';

export const ATTRIBUTES: Attribute[] = [
  { id:'1', name: 'Strength', code: 'STR', value: 10, bonus: +0, counter: 0, notes: 'stubNote' },
  { id:'2', name: 'Agility', code: 'AGI', value: 13, bonus: +1, counter: 0, notes: 'stubNote' },
  { id:'3', name: 'Vitality', code: 'VIT', value: 9, bonus: -1, counter: 0, notes: 'stubNote' },
  { id:'4', name: 'Inteligence', code: 'INT', value: 17, bonus: +3, counter: 0, notes: 'stubNote' },
  { id:'5', name: 'Spirit', code: 'SPR', value: 7, bonus: -2, counter: 0, notes: 'stubNote' }
];

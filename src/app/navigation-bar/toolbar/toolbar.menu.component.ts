import { Component, Input }  from '@angular/core';
import {MatSidenav} from '@angular/material';
import {ToolbarComponent} from './toolbar.component';

@Component({
  template: `
    <div>
      <button mat-icon-button (click)="toggle()">
        <mat-icon>menu</mat-icon>
      </button>
      <span>{{data.page}}</span>
    </div>
  `
})
export class ToolbarMenuComponent extends ToolbarComponent{
  @Input() data: any;

  public toggle(): void {
    (<MatSidenav>this.data.sideNav).toggle();
  }
}

import {Component, OnInit} from '@angular/core';
import {Router} from "@angular/router";
import {User} from "../dtos/User";
import {UserService} from "../services/user.service";
import {SessionService} from '../services/session.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  public user: User;

  constructor(private router: Router,
              private sessionService: SessionService,
              private userService: UserService) {
  }

  ngOnInit() {
    //this.userService.impactDummyUser();
    if (!this.userService.loggedUser) {
      this.router.navigate(['/login']).then();
    } else {
      this.initializeUser();
      this.sessionService.loadingPage("home");
      //localStorage.setItem("page","home");
    }
  }

  initializeUser(): void {
    this.user = this.userService.loggedUser;
  }

  deleteSheet(id: string){
    console.log("Delete User");
    this.userService.updateUser(this.user.id, this.user);
  }

}



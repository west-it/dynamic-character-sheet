import {Sheet} from "./Sheet";

export const SHEETS: Sheet[] = [
  {
    id:'1',
    name: 'hoja 1',
    description: 'Desc 1',
    blocks: [{
      id:'1',
      name: 'bloque 1',
      description: 'descripción',
      visibleColumns: ['value','bonus'],
      attributes: [{
        id: '1',
        name: 'Fuerza',
        code: 'FUE',
        value: 10,
        bonus: 0,
        counter: 0,
        notes: 'stubDesc'
      },
        {
          id: '2',
          name: 'Inteligencia',
          code: 'INT',
          value: 17,
          bonus: 3,
          counter: 0,
          notes: 'stubDesc'
        },
        {
          id: '3',
          name: 'Voluntad',
          code: 'VOL',
          value: 7,
          bonus: -2,
          counter: 0,
          notes: 'stubDesc'
        }
      ]
    }
    ]
  },
  {
    id:'2',
    name: 'hoja 2',
    description: 'Dragons',
    blocks: [{
      id:'1',
      name: 'bloque 1',
      description: 'descripción',
      visibleColumns: ['value','bonus'],
      attributes: [{
        id: '1',
        name: 'Fuerza',
        code: 'FUE',
        value: 10,
        bonus: 0,
        counter: 0,
        notes: 'stubDesc'
      },
        {
          id: '2',
          name: 'Inteligencia',
          code: 'INT',
          value: 17,
          bonus: 3,
          counter: 0,
          notes: 'stubDesc'
        },
        {
          id: '3',
          name: 'Voluntad',
          code: 'VOL',
          value: 7,
          bonus: -2,
          counter: 0,
          notes: 'stubDesc'
        }
      ]
    }
    ]
  },
  {
    id:'3',
    name: 'hoja 3',
    description: 'Mouseguard',
    blocks: [{
      id:'1',
      name: 'bloque 1',
      description: 'descripción',
      visibleColumns: ['value','bonus'],
      attributes: [{
        id: '1',
        name: 'Fuerza',
        code: 'FUE',
        value: 10,
        bonus: 0,
        counter: 0,
        notes: 'stubDesc'
      },
        {
          id: '2',
          name: 'Inteligencia',
          code: 'INT',
          value: 17,
          bonus: 3,
          counter: 0,
          notes: 'stubDesc'
        },
        {
          id: '3',
          name: 'Voluntad',
          code: 'VOL',
          value: 7,
          bonus: -2,
          counter: 0,
          notes: 'stubDesc'
        }
      ]
    }
    ]
  },
  {
    id:'4',
    name: 'hoja 4',
    description: 'Pescadores',
    blocks: [{
      id:'1',
      name: 'bloque 1',
      description: 'descripción',
      visibleColumns: ['value','bonus'],
      attributes: [{
        id: '1',
        name: 'Fuerza',
        code: 'FUE',
        value: 10,
        bonus: 0,
        counter: 0,
        notes: 'stubDesc'
      },
        {
          id: '2',
          name: 'Inteligencia',
          code: 'INT',
          value: 17,
          bonus: 3,
          counter: 0,
          notes: 'stubDesc'
        },
        {
          id: '3',
          name: 'Voluntad',
          code: 'VOL',
          value: 7,
          bonus: -2,
          counter: 0,
          notes: 'stubDesc'
        }
      ]
    }
    ]
  },
  {
    id:'5',
    name: 'hoja 5',
    description: 'Calabozos',
    blocks: [{
      id:'1',
      name: 'bloque 1',
      description: 'descripción',
      visibleColumns: ['value','bonus'],
      attributes: [{
        id: '1',
        name: 'Fuerza',
        code: 'FUE',
        value: 10,
        bonus: 0,
        counter: 0,
        notes: 'stubDesc'
      },
        {
          id: '2',
          name: 'Inteligencia',
          code: 'INT',
          value: 17,
          bonus: 3,
          counter: 0,
          notes: 'stubDesc'
        },
        {
          id: '3',
          name: 'Voluntad',
          code: 'VOL',
          value: 7,
          bonus: -2,
          counter: 0,
          notes: 'stubDesc'
        }
      ]
    }
    ]
  }
];

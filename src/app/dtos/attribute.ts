export interface Attribute {
  id: string;
  name: string;
  code: string;
  value: number;
  bonus: number  ;
  counter: number ;
  notes: string;
}


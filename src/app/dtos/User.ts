import {Sheet} from './Sheet';

export interface User {
  id: string;
  username: string;
  password: string;
  sheets: Sheet[];
}

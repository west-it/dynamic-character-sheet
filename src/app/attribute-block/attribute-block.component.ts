import {Component, Input, OnInit} from '@angular/core';
import {AttributeBlock} from '../dtos/AttributeBlock';

@Component({
  selector: 'app-attribute-block',
  templateUrl: './attribute-block.component.html',
  styleUrls: ['./attribute-block.component.css']
})
export class AttributeBlockComponent implements OnInit {
  @Input() block: AttributeBlock;
  @Input() sheetId: string;

  constructor() { }

  ngOnInit() {
    //console.log(this.block);
  }

}

import {BlockColumns} from './dtos/BlockColumns';
import {AttributeBlock} from './dtos/AttributeBlock';
import {Attribute} from './dtos/attribute';
import {Sheet} from './dtos/Sheet';

export default class Utils {

  static generateId (){
    return Math.floor(Math.random() * 99999999);
  }

  static getColumns(){ //TODO cargar con un .properties
    let columns: BlockColumns[]=[];
    let name= new BlockColumns();
    name.name="Nombre";
    name.code="name";
    columns.push(name);
    let code= new BlockColumns();
    code.name="Código";
    code.code="code";
    columns.push(code);
    let value= new BlockColumns();
    value.name="Valor";
    value.code="value";
    columns.push(value);
    let bonus= new BlockColumns();
    bonus.name="Bonus";
    bonus.code="bonus";
    columns.push(bonus);
    let counter= new BlockColumns();
    counter.name="Contador";
    counter.code="counter";
    columns.push(counter);
    let notes= new BlockColumns();
    notes.name="Notas";
    notes.code="notes";
    columns.push(notes);

    return columns;
  }

  static getColumnsValues(selected: string[]){
    let names:string[]=[];
    if(selected){//selected columns
      this.getColumns().forEach(function (col) {
        selected.forEach(function (selCol) {
          if(selCol===col.code)
            names.push(col.name);
        });
      });
    }else {//all columns
      this.getColumns().forEach(function (col) {
        names.push(col.name);
      });
    }
    return names;
  }

  static getColumnsByCode(cols: string[]){
    let columns: BlockColumns[]=[];
    const allCols= this.getColumns();
    cols.forEach(function (col) {
      allCols.forEach(function (column) {
        if(col===column.code)
          columns.push(column);
      });
    });
    return columns;
  }

  static generateNewSheet() {
    let sheet= new class implements Sheet {
      id: string;
      name: string;
      description: string;
      blocks: AttributeBlock[];
    };
    sheet.name="";
    sheet.description="";
    return sheet;
  }

  static generateNewBlock() {
    let block =new class implements AttributeBlock {
      attributes: Attribute[];
      description: string;
      id: string;
      name: string;
      visibleColumns: string[];
    };
    block.name="";
    block.description="";
    block.attributes=[new class implements Attribute {
      bonus: number;
      code: string;
      counter: number;
      id: string;
      name: string;
      notes: string;
      value: number;
    }];
    block.attributes[0].name="Atributo 1";
    block.attributes[0].value=0;
    block.attributes[0].code="cod";
    block.attributes[0].bonus=0;
    block.attributes[0].counter=0;
    block.attributes[0].notes="";
    block.attributes[0].id=Utils.generateId().toString();

    return block;
  }

}

// This file can be replaced during build by using the `fileReplacements` array.
// `ng build ---prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: 'AIzaSyDGNUN67c0FHTemw20EFO0j2Rmb_Mdb-oM\n',
    authDomain: 'login-ba095.firebaseapp.com',
    databaseURL: 'https://login-ba095.firebaseio.com',
    projectId: 'login-ba095',
    storageBucket: 'login-ba095.appspot.com',
    messagingSenderId: '763399536402'
  }
};

/*
 * In development mode, for easier debugging, you can ignore zone related error
 * stack frames such as `zone.run`/`zoneDelegate.invokeTask` by importing the
 * below file. Don't forget to comment it out in production mode
 * because it will have a performance impact when errors are thrown
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.

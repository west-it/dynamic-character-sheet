import {AttributeBlock} from './AttributeBlock';

export interface Sheet {
  id: string;
  name: string;
  description: string;
  blocks: AttributeBlock[];
}

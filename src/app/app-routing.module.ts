import { NgModule } from '@angular/core';
import {SheetComponent} from './sheet/sheet.component';
import {RouterModule, Routes} from '@angular/router';
import {DashboardComponent} from './dashboard/dashboard.component';
import {LoginComponent} from "./login/login.component";
import {AttributeDetailComponent} from './attribute-detail/attribute-detail.component';
import {BlockDetailComponent} from './block-detail/block-detail.component';

const routes: Routes = [
  {path: '', redirectTo: '/dashboard', pathMatch: 'full'},
  {path: 'dashboard', component: DashboardComponent},
  {path: 'login', component: LoginComponent},
  {path: 'sheet/:id', component: SheetComponent},
  {path: 'detail/:sheetId/:blockId/:id', component: AttributeDetailComponent},
  {path: 'blockDetail/:sheetId/:blockId', component: BlockDetailComponent}

];

@NgModule({
  imports: [ RouterModule.forRoot(routes)],
  exports: [ RouterModule ],
  declarations: []
})
export class AppRoutingModule { }

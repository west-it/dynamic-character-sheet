import {Component, Input, OnInit, ViewChild} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {Sheet} from '../dtos/Sheet';
import {UserService} from '../services/user.service';
import {User} from '../dtos/User';
import {ToolbarService} from '../services/toolbar.service';
import {SessionService} from '../services/session.service';
import {NgForm} from '@angular/forms';
import {BasicDialogComponent} from '../basic-dialog/basic-dialog.component';
import {MatDialog, MatSnackBar} from '@angular/material';
import Utils from '../utils';

@Component({
  selector: 'app-sheet',
  templateUrl: './sheet.component.html',
  styleUrls: ['./sheet.component.css']
})
export class SheetComponent implements OnInit {
  @Input() sheet: Sheet;
  originalSheet:Sheet;
  private user: User;
  @ViewChild(NgForm)sheetForm: NgForm;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private toolbarService: ToolbarService,
    private sessionService:SessionService,
    private userService: UserService,
    public snackBar: MatSnackBar,
    public dialog: MatDialog,
  ) {}

  ngOnInit() {
      this.initializeUser();
      this.getSheet();
      //first set the toolbar, then call to loadingPage
      this.toolbarService.addEditionToolbar("sheet", "Nueva Hoja", "Editar Hoja", this, this.sheet);
      this.sessionService.loadingPage("sheet");
  }

  initializeUser(): void { //TODO llevar esto a una clase base
    this.user = this.userService.loggedUser;
    if (!this.userService.loggedUser) {
      this.router.navigate(['/login']).then();
    }
  }

  getSheet(): void {
    const id = this.route.snapshot.paramMap.get('id');
    if (id === 'new') {
      this.sheet= Utils.generateNewSheet();
    }else{
      this.originalSheet=this.user.sheets.find(x => x.id === id.toString());
      this.sheet= Object.assign({},this.originalSheet);
    }
  }

  save(): void {
    if(this.sheetForm.valid) {
      if (this.sheet.id == null) {
        this.sheet.id = Utils.generateId().toString();
        if(this.user.sheets==null)
          this.user.sheets=[];
        this.user.sheets.push(this.sheet);
      } else {
        this.originalSheet.name = this.sheet.name;
        this.originalSheet.description = this.sheet.description;
      }

      console.log("Saving sheet...");
      this.userService.updateUser(this.user.id, this.user).then(attribute => {
        this.snackBar.open("Hoja guardada!", "Ok", {
          duration: 3000,
        });
      });
    }else{
      this.snackBar.open("Por favor, verifica los datos ingresados...", "Ok", {
        duration: 3000,
      });
    }
  }

  delete(id: string, name:string): void {

    const dialogRef = this.dialog.open(BasicDialogComponent, {
      data: {
        title: "Eliminar hoja",
        content: "Está seguro que desea eliminar la hoja " + name + "? ",
        actions: true
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.user.sheets= this.user.sheets.filter(x => x.id != id.toString());
        this.userService.updateUser(this.user.id, this.user).then(x => {
          this.snackBar.open("La hoja ha sido eliminada", "Ok", {
            duration: 3000,
          });
          this.router.navigate(['/dashboard']).then();
        });
      }else{
        this.snackBar.open("Hubo un problema al eliminar la hoja, por favor inténtalo nuevamente.", "Ok", {
          duration: 3000,
        });
      }
    });

  }

}

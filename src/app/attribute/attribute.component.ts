import {Component, Input, OnInit} from '@angular/core';
import {Attribute} from '../dtos/attribute';
import {BasicDialogComponent} from '../basic-dialog/basic-dialog.component';
import {MatDialog} from '@angular/material';

@Component({
  selector: 'app-attribute',
  templateUrl: './attribute.component.html',
  styleUrls: ['./attribute.component.css']
})


export class AttributeComponent implements OnInit {

  @Input() sheetId: string;
  @Input() blockId: string;
  @Input() attributes: Attribute[];
  @Input() visibleColumns: string[];

  constructor(public dialog: MatDialog) {}

  ngOnInit() {
    //console.log(this.attributes);
    //console.log(this.visibleColumns)
  }

  openDialog(attr:string, notes: string) {
    let dispNotes="";
    if(!notes || notes==="")
      dispNotes="No hay notas disponibles...";
    else
      dispNotes= notes;
    this.dialog.open(BasicDialogComponent, {
      data: {
        title: attr+"> "+ "Notas",
        content: dispNotes,
        actions:false
      }
    });
  }

}

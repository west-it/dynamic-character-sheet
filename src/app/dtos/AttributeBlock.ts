import {Attribute} from './attribute';

export interface AttributeBlock {
  id: string;
  name: string;
  description: string;
  attributes: Attribute[];
  visibleColumns: string[];
}

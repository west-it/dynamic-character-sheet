import { Component, Input }  from '@angular/core';
import {MatSidenav} from '@angular/material';

@Component({
  template: `
    <div>
      <span>{{data.page}}</span>
    </div>
  `
})
export class ToolbarComponent {
  @Input() data: any;

  goBack(): void {
    this.data.location.back();
  }
}
